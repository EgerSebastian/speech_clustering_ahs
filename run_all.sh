#!/bin/bash
# Copyright 2013   Daniel Povey
#      2014-2016   David Snyder
# Apache 2.0.
#
# See README.txt for more info on data required.
# Results (EERs) are inline in comments below.

# This example script is still a bit of a mess, and needs to be
# cleaned up, but it shows you all the basic ingredients.

. cmd.sh
. path.sh
set -e
mfccdir=`pwd`/mfcc
vaddir=`pwd`/mfcc

local/make_malromur.sh /home/ubuntu/malromur data/train

# utils/combine_data.sh data/train data

mfccdir=`pwd`/mfcc
vaddir=`pwd`/mfcc

# utils/validate_data_dir.sh --no-text --no-feats data || exit 1;

set -e
steps/make_mfcc.sh --mfcc-config conf/mfcc.conf --nj 64 --cmd "$train_cmd" \
  data/train exp/make_mfcc $mfccdir

utils/fix_data_dir.sh data/train

sid/compute_vad_decision.sh --nj 40 --cmd "$train_cmd" \
  data/train exp/make_vad $vaddir

# Get male and female subsets of training data.
grep -w m data/train/spk2gender | awk '{print $1}' > foo;
utils/subset_data_dir.sh --spk-list foo data/train data/train_male
grep -w f data/train/spk2gender | awk '{print $1}' > foo;
utils/subset_data_dir.sh --spk-list foo data/train data/train_female
rm foo

# Get smaller subsets of training data for faster training.
utils/subset_data_dir.sh data/train 4000 data/train_4k
utils/subset_data_dir.sh data/train 8000 data/train_8k
utils/subset_data_dir.sh data/train_male 4000 data/train_male_8k
utils/subset_data_dir.sh data/train_female 4000 data/train_female_8k

sid/train_diag_ubm.sh --nj 40 --cmd "$train_cmd" data/train_4k 2048 \
  exp/diag_ubm_2048

sid/train_full_ubm.sh --nj 40 --cmd "$train_cmd" data/train_8k \
  exp/diag_ubm_2048 exp/full_ubm_2048

# Get male and female versions of the UBM in one pass; make sure not to remove
# any Gaussians due to low counts (so they stay matched).  This will be
# more convenient for gender-id.
sid/train_full_ubm.sh --nj 40 --remove-low-count-gaussians false \
  --num-iters 1 --cmd "$train_cmd" \
  data/train_male_8k exp/full_ubm_2048 exp/full_ubm_2048_male &
sid/train_full_ubm.sh --nj 40 --remove-low-count-gaussians false \
  --num-iters 1 --cmd "$train_cmd" \
  data/train_female_8k exp/full_ubm_2048 exp/full_ubm_2048_female &
wait

# Train the iVector extractor for male speakers.
sid/train_ivector_extractor.sh --cmd "$train_cmd -l mem_free=10G,ram_free=10G" \
  --num-iters 5 exp/full_ubm_2048_male/final.ubm data/train_male \
  exp/extractor_2048_male

# The same for female speakers.
sid/train_ivector_extractor.sh --cmd "$train_cmd -l mem_free=10G,ram_free=10G" \
  --num-iters 5 exp/full_ubm_2048_female/final.ubm data/train_female \
  exp/extractor_2048_female

# The script below demonstrates the gender-id script.  We don't really use
# it for anything here, because the SRE 2008 data is already split up by
# gender and gender identification is not required for the eval.
# It prints out the error rate based on the info in the spk2gender file;
# see exp/gender_id_fisher/error_rate where it is also printed.
sid/gender_id.sh --cmd "$train_cmd" --nj 10 exp/full_ubm_2048{,_male,_female} \
  data exp/gender_id_train
# Gender-id error rate is 3.41%

# Extract the iVectors for the training data.
sid/extract_ivectors.sh --cmd "$train_cmd -l mem_free=6G,ram_free=6G" --nj 50 \
  exp/extractor_2048_male data/train_male exp/ivectors_train_male

sid/extract_ivectors.sh --cmd "$train_cmd -l mem_free=6G,ram_free=6G" --nj 50 \
  exp/extractor_2048_female data/train_female exp/ivectors_train_female
