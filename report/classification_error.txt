When calculating the classification error the i-vectors for each speaker is compared with the i-vector
of each utterance.

In Kaldi a special trials file is used when calculating the classification error for speaker verification.
Each line in the file has an id for a speaker i-vector and an id for an utterance i-vector and then
there is a third column that specifies if the utterance was from the speaker or not.

Here is a description of the trials file when using 8000 male utterances and 8000 female utterances:
Trials file for males
targets: 2170
non targets: 499100
Trials file for female
targets: 1935
non targets: 400545

Targets means that the utterance is from the speaker and non target means that the utterance is not from
the speaker.  Because there are much more non targets than targets it is possible yo get a low
total classification error even though the classification error is high for the targets.

Because of this we created a trials file with a much lower number of non target comparisons.
Here is an example:
Trials file for male
targets: 2170
non targets: 2487
Trials file for female
targets: 1935
non targets: 2029

The results when using 8000 male utterances and 8000 female utterances was the following
Male Classification error: 5.9%
Female classification error: 6.7%.

Which is not much different from the classification error when there are many non target comparisons.
