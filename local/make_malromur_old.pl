#!/usr/bin/env perl

use File::Basename;

($data, $info_file ,$out_dir) = @ARGV;

open(TBL, "<$info_file")  or die "cannot open $info_file";

open(GNDR, ">$out_dir/spk2gender") or die "Could not open the output file $out_dir/spk2gender";
open(UTT2SPK, ">$out_dir/utt2spk") or die "Could not open the output file $out_dir/utt2spk";
open(WAV, ">$out_dir/wav.scp") or die "Could not open the output file $out_dir/wav.scp";

while(<TBL>){
  @conv1 = split("	",$_);
  @conv2 = split("_",$_);
  @conv3 = split("-",$_);
  $gender = substr($conv1[3], 0, 1);
  $utt_id = substr($conv1[0], 0, -4);
  $wav = $conv1[0];

  $conv2[1] =~ tr/.-//d;
  $conv3[1] =~ tr/.-//d;
  $utt_id =~ tr/:.//d;
  $utt_id =~ tr/-_//d;

  if($gender ne "u") {
    if(length($conv2[1]) > 20) {
     $spkid = $conv3[1] . $gender;
    } else {
     $spkid = $conv2[1] . $gender;
    }

    if (!defined $seen_spk{$spkid}) {
     $id += 1;
     $seen_spk{$spkid} = 1;
     print GNDRtrain "$spkid $gender\n";
     print GNDRtest "$spkid $gender\n";
    }
    print WAV "$spkid-$utt_id", " $data/wav/$wav\n";
    print UTT2SPK "$spkid-$utt_id", " $spkid\n";

  }
  # @conv == 15 || die "Bad line $_";
}
