import random

# Read in the file
filedata = None
with open('../data/train_shuffledGenders/spk2gender', 'r') as file :
  filedata = file.readlines()

newFiledata = ""

n = len(filedata)
for i in range(0, n):
	if (len(filedata[i]) > 1):
		newFiledata += filedata[i].split(' ')[0] + ' ' + random.choice(['f', 'm']) + '\n'

#print newFiledata
with open('spk2gender', 'w') as file :
  file.write(newFiledata)
