#!/bin/bash


if [ $# -ne 2 ]; then
  echo "Usage: $0 <speech> <data>";
  exit 1;
fi

speech=$1
data=$2

# tmpdir=data/local/tmp
info=$speech/wav_info.txt

if [ ! -f $info ]; then
  echo "$0: $info doesn't exist";
  exit 1
fi

mkdir -p $data/test_male || exit 1;
mkdir -p $data/test_female || exit 1;
mkdir -p $data/train || exit 1;

cmdline="local/make_malromur_split.pl $speech $info $data"

if ! $cmdline; then
  echo "$0 Error running command: $cmdline"
  exit 1
fi

utils/utt2spk_to_spk2utt.pl <$data/train/utt2spk >$data/train/spk2utt
utils/utt2spk_to_spk2utt.pl <$data/test_male/utt2spk >$data/test_male/spk2utt
utils/utt2spk_to_spk2utt.pl <$data/test_female/utt2spk >$data/test_female/spk2utt
utils/fix_data_dir.sh $data/train
utils/fix_data_dir.sh $data/test_male
utils/fix_data_dir.sh $data/test_female

exit 0;
