#!/usr/bin/env perl

use File::Basename;
use File::Copy;

($speaker_ivectors, $utterance_ivectors, $trials_file) = @ARGV;

@speaker_ivec_ids = ();
@utter_ivec_ids = ();

open(SPKIVEC, "<$speaker_ivectors")  or die "cannot open $speaker_ivectors";
while(<SPKIVEC>)
{
	@ivector = split(" ", $_);
	push(@speaker_ivec_ids, $ivector[0]);
}

open(UTTIVEC, "<$utterance_ivectors") or die "cannot open $utterance_ivectors";
while (<UTTIVEC>)
{
	@ivector = split(" ", $_);
	push(@utter_ivec_ids, $ivector[0]);
}

open(TRIALS, ">$trials_file") or die "Could not open the output file $trials_file";

my $speakSize = scalar @speaker_ivec_ids;
my $uttSize = scalar @utter_ivec_ids;

my $targetCount = 0;
my $nontargetCount = 0;

for (my $i = 0; $i < $speakSize; $i++)
{
	for (my $j = 0; $j < $uttSize; $j++)
	{
		# The speaker for the utterance
		my @utter = split("-", $utter_ivec_ids[$j]);
		my $utter_speaker = $utter[0];
		my $targetString = 'nontarget';
		if ($utter_speaker eq $speaker_ivec_ids[$i])
		{
			$targetString = 'target';
			$targetCount += 1;
		}
		else
		{
			$nontargetCount += 1;
		}
		
		print TRIALS $speaker_ivec_ids[$i] . " " . $utter_ivec_ids[$j] . " " . $targetString . "\n";
	}
}

print ("targets: " . $targetCount . "\n");
print ("non targets: " . $nontargetCount . "\n");

