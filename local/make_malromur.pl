#!/usr/bin/env perl

use File::Basename;
use File::Copy;

($data, $info_file, $out_dir) = @ARGV;

open(TBL, "<$info_file")  or die "cannot open $info_file";

open(GNDRtrain, ">$out_dir/train/spk2gender") or die "Could not open the output file $out_dir/spk2gender";
open(UTT2SPKtrain, ">$out_dir/train/utt2spk") or die "Could not open the output file $out_dir/utt2spk";
open(WAVtrain, ">$out_dir/train/wav.scp") or die "Could not open the output file $out_dir/wav.scp";

open(GNDRtest, ">$out_dir/test/spk2gender") or die "Could not open the output file $out_dir/spk2gender";
open(UTT2SPKtest, ">$out_dir/test/utt2spk") or die "Could not open the output file $out_dir/utt2spk";
open(WAVtest, ">$out_dir/test/wav.scp") or die "Could not open the output file $out_dir/wav.scp";

while(<TBL>){
  @conv1 = split("	",$_);
  @conv2 = split("_",$_);
  @conv3 = split("-",$_);
  $gender = substr($conv1[3], 0, 1);
  $utt_id = substr($conv1[0], 0, -4);
  $wav = $conv1[0];

  $conv2[1] =~ tr/.-//d;
  $conv3[1] =~ tr/.-//d;
  $utt_id =~ tr/:.//d;
  $utt_id =~ tr/-_//d;

  if($gender ne "u") {
    if(length($conv2[1]) > 20) {
      $spkid = $conv3[1] . $gender;
    } else {
      $spkid = $conv2[1] . $gender;
    }
    if (!defined $seen_spk{$spkid}) {
      $counter=0;
      $seen_spk{$spkid} = 1;

      print GNDRtrain "$spkid $gender\n";
      print GNDRtest "$spkid $gender\n";

      print WAVtest "$spkid-$utt_id", " $data/wav/$wav\n";
      print UTT2SPKtest "$spkid-$utt_id", " $spkid\n";
    } else {
      $counter++;
      if ($counter < 5) {
        print WAVtest "$spkid-$utt_id", " $data/wav/$wav\n";
        print UTT2SPKtest "$spkid-$utt_id", " $spkid\n";
      } else {
        print WAVtrain "$spkid-$utt_id", " $data/wav/$wav\n";
        print UTT2SPKtrain "$spkid-$utt_id", " $spkid\n";
      }
    }
  }
}
