#!/bin/bash


if [ $# -ne 3 ]; then
  echo "Usage: $0 <speaker_ivectors> <utterance_ivectors> <trials>";
  exit 1;
fi

cmdline="local/make_trials.pl $1 $2 $3"

if ! $cmdline; then
  echo "$0 Error running command: $cmdline"
  exit 1
fi

exit 0;
