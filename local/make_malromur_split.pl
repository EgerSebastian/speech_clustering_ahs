#!/usr/bin/env perl

use File::Basename;
use File::Copy;

($data, $info_file, $out_dir) = @ARGV;

open(TBL, "<$info_file")  or die "cannot open $info_file";

open(GNDRtrain, ">$out_dir/train/spk2gender") or die "Could not open the output file $out_dir/spk2gender";
open(UTT2SPKtrain, ">$out_dir/train/utt2spk") or die "Could not open the output file $out_dir/utt2spk";
open(WAVtrain, ">$out_dir/train/wav.scp") or die "Could not open the output file $out_dir/wav.scp";

open(GNDRtest_male, ">$out_dir/test_male/spk2gender") or die "Could not open the output file $out_dir/spk2gender";
open(UTT2SPKtest_male, ">$out_dir/test_male/utt2spk") or die "Could not open the output file $out_dir/utt2spk";
open(WAVtest_male, ">$out_dir/test_male/wav.scp") or die "Could not open the output file $out_dir/wav.scp";

open(GNDRtest_female, ">$out_dir/test_female/spk2gender") or die "Could not open the output file $out_dir/spk2gender";
open(UTT2SPKtest_female, ">$out_dir/test_female/utt2spk") or die "Could not open the output file $out_dir/utt2spk";
open(WAVtest_female, ">$out_dir/test_female/wav.scp") or die "Could not open the output file $out_dir/wav.scp";

while(<TBL>){
  @conv1 = split("	",$_);
  # @conv2 = split("_",$_);
  @conv3 = split("-",$_);
  $gender = substr($conv1[3], 0, 1);
  $utt_id = substr($conv1[0], 0, -4);
  $wav = $conv1[0];

  # $conv2[1] =~ tr/.-//d;
  $conv3[1] =~ tr/._//d;
  $utt_id =~ tr/:.//d;
  $utt_id =~ tr/-_//d;

  if($gender ne "u") {
    #if(length($conv2[1]) > 20) {
    #  $spkid = $conv3[1] . $gender . $conv1[4];
    #} else {
    #  $spkid = $conv2[1] . $gender . $conv1[4];
    #}
    $spkid = $conv3[1] . $gender;
    if (!defined $seen_spk{$spkid}) {
      print "$counter";
      $counter=0;
      $seen_spk{$spkid} = 1;

      print GNDRtrain "$spkid $gender\n";
      if ($gender eq 'm') {
        print GNDRtest_male "$spkid $gender\n";
        print WAVtest_male "$spkid-$utt_id", " $data/wav/$wav\n";
        print UTT2SPKtest_male "$spkid-$utt_id", " $spkid\n";
      } else {
        print GNDRtest_female "$spkid $gender\n";
        print WAVtest_female "$spkid-$utt_id", " $data/wav/$wav\n";
        print UTT2SPKtest_female "$spkid-$utt_id", " $spkid\n";
      }
    } else {
      $counter++;
      if ($counter < 10) {
        if ($gender eq 'm') {
          print WAVtest_male "$spkid-$utt_id", " $data/wav/$wav\n";
          print UTT2SPKtest_male "$spkid-$utt_id", " $spkid\n";
        } else {
          print WAVtest_female "$spkid-$utt_id", " $data/wav/$wav\n";
          print UTT2SPKtest_female "$spkid-$utt_id", " $spkid\n";
        }
      } else {
          print WAVtrain "$spkid-$utt_id", " $data/wav/$wav\n";
          print UTT2SPKtrain "$spkid-$utt_id", " $spkid\n";
      }
    }
  }
}
