#!/bin/bash


if [ $# -ne 2 ]; then
  echo "Usage: $0 <speech> <data>";
  exit 1;
fi

speech=$1
data=$2

# tmpdir=data/local/tmp
info=$speech/wav_info.txt

if [ ! -f $info ]; then
  echo "$0: $info doesn't exist";
  exit 1
fi

mkdir -p $data || exit 1;

cmdline="local/make_malromur.pl $speech $info $data"

if ! $cmdline; then
  echo "$0 Error running command: $cmdline"
  exit 1
fi

utils/utt2spk_to_spk2utt.pl <$data/utt2spk >$data/spk2utt
utils/fix_data_dir.sh $data

exit 0;
