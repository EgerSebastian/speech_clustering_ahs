. cmd.sh
. path.sh
set -e
	
# Results for Male:
cat exp/trials_male | awk '{print $1, $2}' | \
  ivector-compute-dot-products - \
  scp:exp/ivectors_train_male/spk_ivector.scp \
  'ark:ivector-normalize-length scp:exp/ivectors_test_male/ivector.scp ark:- |' \
  foo
local/score_hoddi_simple.sh exp/trials_male foo

# Results for Female:
cat exp/trials_female | awk '{print $1, $2}' | \
  ivector-compute-dot-products - \
  scp:exp/ivectors_train_female/spk_ivector.scp \
  'ark:ivector-normalize-length scp:exp/ivectors_test_female/ivector.scp ark:- |' \
  foo
local/score_hoddi_simple.sh exp/trials_female foo

