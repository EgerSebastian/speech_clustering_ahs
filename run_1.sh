#!/bin/bash
# Copyright 2013   Daniel Povey
#      2014-2016   David Snyder
# Apache 2.0.
#
# See README.txt for more info on data required.
# Results (EERs) are inline in comments below.

# This example script is still a bit of a mess, and needs to be
# cleaned up, but it shows you all the basic ingredients.

. cmd.sh
. path.sh
set -e
mfccdir=`pwd`/mfcc
vaddir=`pwd`/mfcc

local/make_malromur.sh /home/ubuntu/malromur data

# utils/combine_data.sh data/train data

mfccdir=`pwd`/mfcc
vaddir=`pwd`/mfcc

utils/validate_data_dir.sh --no-text --no-feats data/train || exit 1;
utils/validate_data_dir.sh --no-text --no-feats data/test_male || exit 1;
utils/validate_data_dir.sh --no-text --no-feats data/test_female || exit 1;

set -e
steps/make_mfcc.sh --mfcc-config conf/mfcc.conf --nj 40 --cmd "$train_cmd" \
   data/train exp/make_mfcc $mfccdir
steps/make_mfcc.sh --mfcc-config conf/mfcc.conf --nj 40 --cmd "$train_cmd" \
   data/test_male exp/make_mfcc $mfcdir
steps/make_mfcc.sh --mfcc-config conf/mfcc.conf --nj 40 --cmd "$train_cmd" \
   data/test_female exp/make_mfcc $mfcdir
