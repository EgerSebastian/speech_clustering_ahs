#!/bin/bash
# Copyright 2013   Daniel Povey
#      2014-2016   David Snyder
# Apache 2.0.
#
# See README.txt for more info on data required.
# Results (EERs) are inline in comments below.

# This example script is still a bit of a mess, and needs to be
# cleaned up, but it shows you all the basic ingredients.

speech=/home/ubuntu/malromur
info=$speech/wav_info.txt

mkdir -p $speech/test || exit 1;

cmdline="local/make_test.pl $speech $info $speech/test"

if ! $cmdline; then
  echo "$0 Error running command: $cmdline"
  exit 1
fi
