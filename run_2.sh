#!/bin/bash
# Copyright 2013   Daniel Povey
#      2014-2016   David Snyder
# Apache 2.0.
#
# See README.txt for more info on data required.
# Results (EERs) are inline in comments below.

# This example script is still a bit of a mess, and needs to be
# cleaned up, but it shows you all the basic ingredients.

. cmd.sh
. path.sh
set -e
mfccdir=`pwd`/mfcc
vaddir=`pwd`/mfcc

utils/fix_data_dir.sh data/train

sid/compute_vad_decision.sh --nj 40 --cmd "$train_cmd" \
  data/train exp/make_vad $vaddir
sid/compute_vad_decision.sh --nj 40 --cmd "$train_cmd" \
  data/test_male exp/make_vad $vaddir
sid/compute_vad_decision.sh --nj 40 --cmd "$train_cmd" \
  data/test_female exp/make_vad $vaddir

# Get male and female subsets of training data.
grep -w m data/train/spk2gender | awk '{print $1}' > foo;
utils/subset_data_dir.sh --spk-list foo data/train data/train_male
grep -w f data/train/spk2gender | awk '{print $1}' > foo;
utils/subset_data_dir.sh --spk-list foo data/train data/train_female
rm foo

# Get smaller subsets of training data for faster training.
utils/subset_data_dir.sh data/train 4000 data/train_4k
utils/subset_data_dir.sh data/train 8000 data/train_8k
utils/subset_data_dir.sh data/train_male 4000 data/train_male_8k
utils/subset_data_dir.sh data/train_female 4000 data/train_female_8k

sid/train_diag_ubm.sh --nj 40 --cmd "$train_cmd" data/train_4k 2048 \
  exp/diag_ubm_2048

sid/train_full_ubm.sh --nj 40 --cmd "$train_cmd" data/train_8k \
  exp/diag_ubm_2048 exp/full_ubm_2048
