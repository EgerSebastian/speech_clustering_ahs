#!/bin/bash
# Copyright 2013   Daniel Povey
#      2014-2016   David Snyder
# Apache 2.0.
#
# See README.txt for more info on data required.
# Results (EERs) are inline in comments below.

# This example script is still a bit of a mess, and needs to be
# cleaned up, but it shows you all the basic ingredients.

. cmd.sh
. path.sh
set -e
mfccdir=`pwd`/mfcc
vaddir=`pwd`/mfcc

# Extract the iVectors for the training data.
sid/extract_ivectors.sh --cmd "$train_cmd -l mem_free=62G,ram_free=62G" --nj 16 \
  exp/extractor_2048_male data/train_male exp/ivectors_train_male

sid/extract_ivectors.sh --cmd "$train_cmd -l mem_free=62G,ram_free=62G" --nj 16 \
  exp/extractor_2048_female data/train_female exp/ivectors_train_female

sid/extract_ivectors.sh --cmd "$train_cmd -l mem_free=62G,ram_free=62G" --nj 16 \
  exp/extractor_2048_male data/test_male \
  exp/ivectors_test_male

sid/extract_ivectors.sh --cmd "$train_cmd -l mem_free=62G,ram_free=62G" --nj 16 \
  exp/extractor_2048_female data/test_female \
  exp/ivectors_test_female


echo 'Finished extracting the i-Vectors'
exit 1

trials=data/female-trials/female.trials

cat $trials | awk '{print $1, $2}' | \
  ivector-compute-dot-products - \
  scp:exp/ivectors_test_male/spk_ivector.scp \
  'ark:ivector-normalize-length scp:exp/ivectors_train_male/ivector.scp ark:- |' \
  foo
