# This script makes the trials files

echo 'Making trials file for male'
local/make_trials.sh \
	exp/ivectors_train_male/spk_ivector.scp \
	exp/ivectors_test_male/ivector.scp \
	exp/trials_male

echo 'Making trials file for female'
local/make_trials.sh \
	exp/ivectors_train_female/spk_ivector.scp \
	exp/ivectors_test_female/ivector.scp \
	exp/trials_female
	
