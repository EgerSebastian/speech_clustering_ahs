#!/bin/bash
# Copyright 2013   Daniel Povey
#      2014-2016   David Snyder
# Apache 2.0.
#
# See README.txt for more info on data required.
# Results (EERs) are inline in comments below.

# This example script is still a bit of a mess, and needs to be
# cleaned up, but it shows you all the basic ingredients.

. cmd.sh
. path.sh
set -e
mfccdir=`pwd`/mfcc
vaddir=`pwd`/mfcc

# Get male and female versions of the UBM in one pass; make sure not to remove
# any Gaussians due to low counts (so they stay matched).  This will be
# more convenient for gender-id.
sid/train_full_ubm.sh --nj 40 --remove-low-count-gaussians false \
  --num-iters 1 --cmd "$train_cmd" \
  data/train_male_8k exp/full_ubm_2048 exp/full_ubm_2048_male &
sid/train_full_ubm.sh --nj 40 --remove-low-count-gaussians false \
  --num-iters 1 --cmd "$train_cmd" \
  data/train_female_8k exp/full_ubm_2048 exp/full_ubm_2048_female &
wait
