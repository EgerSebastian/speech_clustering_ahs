. cmd.sh
. path.sh
set -e

echo 'Making trials file for male'
local/make_trials.sh \
	exp/ivectors_train_male/spk_ivector.scp \
	exp/ivectors_test_male/ivector.scp \
	exp/trials_male

echo 'Making trials file for female'
local/make_trials.sh \
	exp/ivectors_train_female/spk_ivector.scp \
	exp/ivectors_test_female/ivector.scp \
	exp/trials_female

# Note: speaker-level i-vectors have already been length-normalized
# by sid/extract_ivectors.sh, but the utterance-level test i-vectors
# have not.
cat exp/trials_male | awk '{print $1, $2}' | \
  ivector-compute-dot-products - \
  scp:exp/ivectors_sre08_train_short2_female/spk_ivector.scp \
  'ark:ivector-normalize-length scp:exp/ivectors_sre08_test_short3_female/ivector.scp ark:- |' \
  foo
local/score_sre08.sh exp/trials_male foo

# Results for Female:
# Scoring against data/sre08_trials/short2-short3-female.trials
#  Condition:      0      1      2      3      4      5      6      7      8
#        EER:  12.70  20.09   4.78  19.08  16.37  15.87  10.42   7.10   7.89
cat exp/trials_female | awk '{print $1, $2}' | \
  ivector-compute-dot-products - \
  scp:exp/ivectors_sre08_train_short2_male/spk_ivector.scp \
  'ark:ivector-normalize-length scp:exp/ivectors_sre08_test_short3_male/ivector.scp ark:- |' \
  foo
local/score_sre08.sh exp/trials_female foo
